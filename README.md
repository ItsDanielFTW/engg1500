# Workshop Team 4 Robot
A robot project for ENGG1500 S1 2022
## Members 

 * Daniel Yarrington
 * Alec Harrison
 * Fletcher Carlton
 * Jackson Thoroughgood

## Modules
### Summary
Modules in this project are for interfacing between the **Raspberry Pi Pico** and individual components such as the IR sensors and motors.
## How to connect to this repo.
### What you'll need

 - Sourcetree
- [A BitBucket account](bitbucket.org)
 - PyCharm Community Edition
