# code to make robot follow a line

from machine import Pin, ADC
from modules.Motor import Motor
from time import sleep

# Initialising
# Motors
motor_left = Motor("left", 8, 9, 6)
motor_right = Motor("right", 10, 11, 7)

# Line Sensors
IR1 = Pin(26, Pin.IN).value()
IR2 = Pin(28, Pin.IN).value()

while True:
    print(IR1)
    print(IR2)
    sleep(0.1)

    # motor_left.set_forwards()
    # motor_right.set_forwards()
    # motor_left.duty(50)
    # motor_right.duty(50)

    # if IR1 < IR2:
    #     motor_left.set_forwards()
    #     motor_right.set_backwards()
    #     motor_left.duty(50)
    #     motor_right.duty(50)
    # # elif IR1 == True and IR2 == False:      # turn right
    # elif IR1 > IR2:
    #     motor_right.set_forwards()
    #     motor_left.set_backwards()
    #     motor_left.duty(50)
    #     motor_right.duty(50)
    # else:
    #     motor_left.duty(0)
    #     motor_right.duty(0)
