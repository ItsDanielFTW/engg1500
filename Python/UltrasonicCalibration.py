from machine import Pin
from modules.Ultrasonic import HCSR04 as sonic
from time import sleep

# initialise ultrasonic sensor here
TRIG = 3
ECHO = 2
ultrasonic_sensor = sonic(TRIG, ECHO)
dist = ultrasonic_sensor.read()

while True:
    # if ultrasonic_sensor <= 20:
    if dist == True:
        print("Object: {}".format(ultrasonic_sensor))
        sleep(1)
