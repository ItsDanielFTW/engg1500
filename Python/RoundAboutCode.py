from machine import Pin, ADC
from modules.Motor import Motor
from modules.Ultrasonic import HCSR04 as sonic
from modules.Encoder import Encoder
from time import sleep

# Initialisation
# IR Sensors
IR1 = Pin(26, Pin.IN).value()
IR2 = Pin(28, Pin.IN).value()

# Encoders
ENC_L = 18
ENC_R = 19
enc = Encoder(ENC_L, ENC_R)

# Motors
motor_left = Motor("left", 8, 9, 6)
motor_right = Motor("right", 10, 11, 7)


