Pseudocode for Distract Line
// With current line follow code you're not going to be able to ignore other lines
// Could send robot straight ahead but we don't know what orientation the robot will be in

Line follow code
If Encoder Right > Certain limit (limit is encoder value that is retrieved when right sensor rotates to distract line):
	Stop Right motor
	Turn left motor -50 pwm
	Set left motor to 50 pwm
	Set right motor to 50 pwm
	Line follow code
If Encoder Left > Certain limit (limit is encoder value that is retrieved when left sensor rotates to distract line):
	Stop Left motor
	Turn right motor -50 pwm
	Set left motor to 50 pwm
	Set right motor to 50 pwm
	Line follow code

// Big problem is that if other one stops then line sensor code may be wrong since robot is in another position
// Another way to do it may be to set robot to straight and then just let it go forward (but like how does robot know
// it's going straight and when does it stop?)

