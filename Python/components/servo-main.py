# Servo component code by Daniel | Derived from Lab Sheet 3
# Last Updated: 03-05-2022

import time
from machine import Pin, PWM

pwm = PWM(Pin(15))
pwm.frequency(50)


# Define function to set position
def setServoAngle(angle):
    position = int(8000 * (angle / 180) + 1000)  # Convert Angle into [1000,9000]
    # ...range
    pwm.duty_u16(position)


while True:
    # Sweep between 0 and 180 degrees
    for pos in range(0, 180, 5):
        setServoAngle(pos)
        time.sleep(0.05)
    for pos in range(180, 0, -5):
        setServoAngle(pos)
        time.sleep(0.05)
