from machine import Pin
import time

green_LED = Pin(25, Pin.OUT)

while True:
    green_LED.on()
    time.sleep(1)
    green_LED.off()
    time.sleep(1)
