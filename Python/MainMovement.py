from modules.Motor import Motor
from machine import Pin
from time import sleep

motor_left = Motor("left", 8, 9, 6)
motor_right = Motor("right", 10, 11, 7)

while True:
    motor_left.set_forwards()
    motor_right.set_forwards()
    motor_right.duty(50)
    motor_left.duty(50)
    sleep(1)
