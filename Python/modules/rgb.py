from machine import I2C, Pin
from time import sleep
from APDS9960LITE import APDS9960LITE

#Init I2C bus
i2c = I2C(0, scl=Pin(17), sda=Pin(16))

#Init APDS9960
apds9960=APDS9960LITE(i2c)     #create sensor object
apds9960.prox.enableSensor()  #Send i2c command to enable sensor
sleep(0.1)

while True
    proximity_measurement = apds9960.prox.proximityLevel # Read the proximity

    print(proximity_measurement)
    sleep(0.2)