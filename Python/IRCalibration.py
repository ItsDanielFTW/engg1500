# testing IR Sensors to see if they actually work or are dead

from machine import Pin, ADC
from time import sleep

IR1 = Pin(26, Pin.IN).value()
IR2 = Pin(28, Pin.IN).value()

while True:
    print(IR1)
    print(IR2)
    sleep(0.1)
